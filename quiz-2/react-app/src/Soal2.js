import React, { Component } from 'react'

class Soal2 extends Component {
    render() {
        const data = [
            {
                nama: "Bejo",
                pekerjaan: "Programmer Frontend",
                hobi: "Ngoding Santai",
            },
            {
                nama: "Untung",
                pekerjaan: "Programmer Backend",
                hobi: "Ngopi",
            },
            {
                nama: "Budi",
                pekerjaan: "Programmer Fullstack",
                hobi: "Mancing Ikan",
            },
        ];
        const listData = data.map(({nama, pekerjaan, hobi}, index) => {
            return(
                <div key={index}>
                  <h1>No - {index + 1}</h1>
                  <p>Nama : {nama}</p>
                  <p>Pekerjaan : {pekerjaan}</p>
                  <p>Hobi : {hobi}</p>
                </div>
            )
        })
        return (
            <div>
                {listData}
            </div>
        )
    }
}

export default Soal2