import React, { Component } from "react";

class Soal4 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nama: "",
      gaji: 0,
      tglMasuk: "",
      gender: "",
      skills: [],
      alamat: "",
    };
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const { nama, gaji, alamat } = this.state;
    alert(
      `Namaku adalah ${nama}, aku memiliki gaji bulanan sebesar ${gaji}. Dan rumahku di ${alamat}`
    );
  };

  handlePick = (e) => {
    let skills = [...this.state.skills];
    let index = skills.findIndex((elemen) => e.target.value === elemen);
    if (index > 0) {
      skills = [
        ...skills.slice(0, index),
        ...skills.slice(index + 1, skills.length),
      ];
    } else if (index === 0) {
      skills = [...skills.slice(index + 1, skills.length)];
    } else {
      skills.push(e.target.value);
    }
    this.setState({ skills });
  };

  render() {
    const { nama, gaji, tglMasuk, gender, skills, alamat } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <label>Nama</label>
          <div>
            <input
              type="text"
              name="nama"
              value={nama}
              onChange={this.handleChange}
              required
            />
          </div>
        </div>
        <div>
          <label>Gaji</label>
          <div>
            <input
              type="number"
              name="gaji"
              value={gaji}
              onChange={this.handleChange}
              required
            />
          </div>
        </div>
        <div>
          <label>Tanggal Masuk Kerja</label>
          <div>
            <input
              type="date"
              name="tglMasuk"
              value={tglMasuk}
              onChange={this.handleChange}
              required
            />
          </div>
        </div>
        <div>
          <label>Gender</label>
          <div>
            <input
              type="radio"
              name="gender"
              value="Laki-laki"
              checked={gender === "Laki-laki"}
              onChange={this.handleChange}
            />{" "}
            Laki - laki
            <input
              type="radio"
              name="gender"
              value="Perempuan"
              checked={gender === "Perempuan"}
              onChange={this.handleChange}
            />{" "}
            Perempuan
          </div>
        </div>
        <div>
          <select onChange={this.handlePick} value={skills} multiple required>
            <option value="">Pilih Skill Anda</option>
            <option value="frontend">Front End Developer</option>
            <option value="backend">Back End Developer</option>
            <option value="fullstack">Full Stack Developer</option>
          </select>
        </div>
        <div>
          <label>Alamat</label>
          <div>
            <textarea
              name="alamat"
              onChange={this.handleChange}
              value={alamat}
              required
            >
              {alamat}
            </textarea>
          </div>
        </div>
        <br />
        <button type="submit">Submit</button>
      </form>
    );
  }
}

export default Soal4;
