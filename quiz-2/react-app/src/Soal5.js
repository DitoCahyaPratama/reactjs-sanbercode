import React, { Component } from "react";

class Soal5 extends Component {
  constructor(props) {
    super(props);

    this.inputNama = React.createRef()
    this.inputGaji = React.createRef()
    this.inputTgl = React.createRef()
    this.inputGender = React.createRef()
    this.inputSkills = React.createRef()
    this.inputAlamat = React.createRef()
  }

  handleSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    console.log(
        `Namaku adalah ${this.inputNama.current.value}, 
        aku memiliki gaji bulanan sebesar ${this.inputGaji.current.value}, 
        aku masuk kerja pada tanggal ${this.inputTgl.current.value}.
        Dan rumahku di ${this.inputAlamat.current.value}
        ${this.inputGender.current.checked}
        ${this.inputSkills.current.value}`
      );
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <label>Nama</label>
          <div>
            <input type="text" name="nama" ref={this.inputNama} required />
          </div>
        </div>
        <div>
          <label>Gaji</label>
          <div>
            <input type="number" name="gaji" ref={this.inputGaji} required />
          </div>
        </div>
        <div>
          <label>Tanggal Masuk Kerja</label>
          <div>
            <input type="date" name="tglMasuk" ref={this.inputTgl} required />
          </div>
        </div>
        <div>
          <label>Gender</label>
          <div>
            <input type="radio" name="gender" value="Laki-laki" ref={this.inputGender} /> Laki - laki
            <input type="radio" name="gender" value="Perempuan" ref={this.inputGender} /> Perempuan
          </div>
        </div>
        <div>
          <select ref={this.inputSkills} multiple required>
            <option value="">Pilih Skill Anda</option>
            <option value="frontend">Front End Developer</option>
            <option value="backend">Back End Developer</option>
            <option value="fullstack">Full Stack Developer</option>
          </select>
        </div>
        <div>
          <label>Alamat</label>
          <div>
            <textarea ref={this.inputAlamat} name="alamat" required></textarea>
          </div>
        </div>
        <br />
        <button type="submit">Submit</button>
      </form>
    );
  }
}

export default Soal5;
