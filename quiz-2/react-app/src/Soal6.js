import React, { Component } from 'react'

class DistanceInput extends Component {
    _handleChange = (e) => {
        this.props.onChangeTime(e.target.value)
    }
    render() {
        const{ distanceLabel, distance } = this.props
        return (
            <>
                <label>{(distanceLabel === "km") ? "Kilometer" : (distanceLabel === "m") ? "Meter" : "Milimeter"}</label>
                <div>
                    <input type="number" value={distance} onChange={this._handleChange}/>
                </div>
            </>
        )
    }
}

class Soal6 extends Component {
    constructor(props){
        super(props)
        this.state = {
            kilometer: '', 
            meter: '', 
            milimeter: ''
        }
    }

    handleSubmit = (e) => {
        e.preventDefault()
        alert(`milimeter: ${this.state.milimeter} sama dengan meter: ${this.state.meter} sama dengan kilometer: ${this.state.kilometer}`);
    }

    onChangeMilimeters = (milimeter) => {
        let meter = (parseFloat(milimeter) / 1000).toString()
        let kilometer = (parseFloat(milimeter) / 1000000).toString()
        this.setState({ milimeter, meter, kilometer })
    }

    onChangeMeters = (meter) => {
        let milimeter = (parseFloat(meter) * 1000).toString()
        let kilometer = (parseFloat(meter) / 1000).toString()
        this.setState({ milimeter, meter, kilometer })
    }

    onChangeKilometers = (kilometer) => {
        let milimeter = (parseFloat(kilometer) * 1000000).toString()
        let meter = (parseFloat(kilometer) * 1000).toString()
        this.setState({ milimeter, meter, kilometer })
    }

    render() {
        const {kilometer, meter, milimeter} = this.state
        return (
            <form onSubmit={this.handleSubmit}>
                <DistanceInput distanceLabel="mm" distance={milimeter} onChangeTime={this.onChangeMilimeters} />
                <DistanceInput distanceLabel="m" distance={meter} onChangeTime={this.onChangeMeters} />
                <DistanceInput distanceLabel="km" distance={kilometer} onChangeTime={this.onChangeKilometers} />
                <br />
                <button type="submit">Convert!</button>
            </form>
        )
    }
}

export default Soal6