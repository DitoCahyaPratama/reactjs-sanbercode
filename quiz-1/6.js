const numbers = [1, 4, 5, 1, 2, 10, 12, 15, 11, 13, 11, 5].filter((num) => num % 2 == 1).filter((item,pos, arr) => arr.indexOf(item) == pos);
const reducer = (accumulator, currentValue) => accumulator + currentValue;
const data = numbers.reduce(reducer);
console.log(data);