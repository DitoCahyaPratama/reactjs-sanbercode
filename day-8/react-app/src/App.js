import React, { Component } from 'react'

export default class App extends Component {
  constructor(){
    super()
    this.state = {
      counter: 0
    }
  }
  countersetState = () => {
    this.setState({
      counter: this.state.counter + 1
    })
  }
  render() {
    return (
      <div>
        <button onClick={this.countersetState}>Klik untuk menambah counter</button>
        <h1>{this.state.counter}</h1>
      </div>
    )
  }
}
