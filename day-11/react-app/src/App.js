import React, { Component } from "react";
import "./App.css";

class ProductTypeRow extends Component {
  render() {
    const type = this.props.type;
    return (
      <tr>
        <th colSpan="2" style={{ textAlign:"left", fontSize:"28px", background: '#333' }}>{type}</th>
      </tr>
    );
  }
}

class ProductRow extends Component {
  render() {
    const product = this.props.product;
    const name = product.ready ? product.nama : <span style={{color: 'red'}}>{product.nama}</span>;
    const harga = product.ready ? product.harga : <span style={{color: 'red'}}>{product.harga}</span>;
    
    return(
      <tr>
        <td class="pad">{name}</td>
        <td>{harga}</td>
      </tr>
    )
  }
}

class ProductTable extends Component {
  render() {
    const filterText = this.props.filterText;
    const readyOnly = this.props.inReadyOnly;
    
    const rows = [];
    let lastType = null;

    this.props.products.forEach((product) => {
      const nama = product.nama.toLowerCase()
      if (nama.indexOf(filterText) === -1) {
        return;
      }
      if (readyOnly && !product.ready) {
        return;
      }
      if (product.type !== lastType) {
        rows.push(<ProductTypeRow type={product.type} key={product.type} />);
      }
      rows.push(<ProductRow product={product} key={product.nama} />);
      lastType = product.type;
    });
    return (
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Harga</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
    );
  }
}

class SearchBar extends Component {

  handleFilterTextChange = (e) => {
    this.props.onFilterTextChange(e.target.value);
  };

  handleInReadyChange = (e) => {
    this.props.onReadyChange(e.target.checked);
  };

  render() {
    return (
      <form>
        <input
          className="input-search"
          type="text"
          placeholder="Search..."
          value={this.props.filterText}
          onChange={this.handleFilterTextChange}
        />
        <p>
          <input
            type="checkbox"
            checked={this.props.readyOnly}
            onChange={this.handleInReadyChange}
          />{" "}
          Tampilkan product yang ready
        </p>
      </form>
    );
  }
}

class FilterableProductTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: "",
      readyOnly: false,
    };
  }

  handleFilterTextChange = (filterText) => {
    this.setState({
      filterText: filterText.toLowerCase(),
    });
  };

  handleInReadyChange = (inReadyOnly) => {
    this.setState({
      readyOnly: inReadyOnly,
    });
  };

  render() {
    return (
      <div className="App-header">
        <SearchBar
          filterText={this.state.filterText}
          readyOnly={this.state.readyOnly}
          onFilterTextChange={this.handleFilterTextChange}
          onReadyChange={this.handleInReadyChange}
        />
        <ProductTable
          products={this.props.products}
          filterText={this.state.filterText}
          inReadyOnly={this.state.readyOnly}
        />
      </div>
    );
  }
}

class App extends Component {
  render() {
    const PRODUCTS = [
      { type: "Mobil", harga: "Rp 148.000.000", ready: true, nama: "Brio" },
      { type: "Mobil", harga: "Rp 205.500.000", ready: true, nama: "Mobilio" },
      { type: "Mobil", harga: "Rp 252.500.000", ready: false, nama: "Jazz" },
      { type: "Motor", harga: "Rp 16.000.000", ready: true, nama: "Beat" },
      { type: "Motor", harga: "Rp 29.000.000", ready: false, nama: "PCX" },
      { type: "Motor", harga: "Rp 35.000.000", ready: true, nama: "CBR150R" },
    ];
    return (
      <div className="app">
        <FilterableProductTable products={PRODUCTS} />
      </div>
    );
  }
}

export default App;
