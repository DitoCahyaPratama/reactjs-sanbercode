/**
 *
 * "this" keyword
 *
 */

const kucing = {
    nama: 'Anggora',
    love: 'bola',
    meong() {
        // console.log(this.nama + ' suka ' + this.love);
        console.log(this);
    }
}

kucing.meong();






























/**
*
* konsep "binding" untuk "this"
*
*/

const meong = kucing.meong.bind(kucing);
meong();
































/**
*
* Pengenalan Arrow Function
*
*/

// function getKucingName(name) {
//     console.log(`${name} Anggora`)
// }

// const getKucingName = function (name) {
//     console.log(`${name} Anggora`)
// }

const getKucingName = () => console.log(`Anggora`)

// setInterval(() => console.log('hai'), 1000);































/**
*
* Arrow Function dan "this"
*
*/


const kucing2 = {
    name: 'Anggora',
    getName() {
        setTimeout(() => console.log(this), 2000)
    }
}

// kucing2.getName();


// Tugas 2


// function getNamaLengkap(namaDepan, namaBelakang) {
//     return `${namaDepan} ${namaBelakang}`;
//  }
  
//  function getNamaDepan(namaLengkap) {
//     return namaLengkap.split(' ')[0];
//  }
  
//  function consoleHai() {
//     console.log('hai');
//  }
 



const getNamaLengkap = (namaDepan, namaBelakang) => {
    return `${namaDepan} ${namaBelakang}`;
}

const getNamaDepan = (namaLengkap) => {
    return namaLengkap.split(' ')[0];
}

const consoleHai = () => console.log('hai');

console.log(getNamaLengkap('Dito', 'Cahya Pratama'));




//Soal no 3

const sebuahObject = {
    name: 'Bejo Jhonson',
    getName() {
        return this.name;
    }
 }
  
 const sebuahObjectLain = {
    name: 'Bejo Jhonson',
    getName: () => {
        this.name = 'Bejo Jhonson';
        return this.name;
    } 
 }
 

 console.log(sebuahObjectLain.getName());

 const profil = {
    namaDepan: 'Bejo',
    namaBelakang: 'Jhonson',
    getName() {
        this.name = `${this.namaDepan} ${this.namaBelakang}`;
        return this.name;
    }
 }

