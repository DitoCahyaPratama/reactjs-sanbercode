// const _handleSubstract = () => ({
//         type: 'SUBSTRACT COUNTER',          
// })

// const _handleAdd = () => ({
//         type: 'ADD COUNTER',          
// })

const apikey = `60622cd729a94909bbb4629582cc892f`;

export function incrementAsync() {
    return async (dispatch) => {
        const response = await fetch(`http://newsapi.org/v2/everything?q=bitcoin&from=2020-06-03&sortBy=publishedAt&apiKey=${apikey}`)
        setTimeout(() => {
            dispatch({
                type: 'ADD COUNTER',
                payload: response.json(),
            })
        }, 5000)
        // setTimeout(() => {
        //     dispatch(_handleAdd())
        // }, 1000)
    }
}

export function decrementAsync(){
    return async (dispatch) => {
        const response = await fetch(`http://newsapi.org/v2/everything?q=bitcoin&from=2020-06-03&sortBy=publishedAt&apiKey=${apikey}`)
        setTimeout(() => {
            dispatch({
                type: 'SUBSTRACT COUNTER',
                payload: response.json(),
            })
        }, 5000)
        // setTimeout(() => {
        //     dispatch(_handleSubstract())
        // }, 1000)
    }
}