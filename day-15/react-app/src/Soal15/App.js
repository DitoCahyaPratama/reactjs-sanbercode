import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { incrementAsync, decrementAsync } from '../actions'

function App() {
    const dispatch = useDispatch()
    const counters = useSelector(state => state.counters.counter)

	const _handleAdd = () => {
		dispatch(incrementAsync())
	}

	const _handleSubstract = () => {
		dispatch(decrementAsync())
	}

	return (
        <div className="App App-header">
            <div>Counter : {counters}</div>
            <div>
                <button onClick={_handleSubstract}>Substract</button>
                <button onClick={_handleAdd}>Add</button>
            </div>
        </div>
    )
}

export default App