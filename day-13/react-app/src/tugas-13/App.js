import React from 'react';
import '../App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Home, About, Users } from './views'
import { Nav } from './components'

function App() {
	return (
		<Router>
			<div className="App">
                <Nav />
                <Switch>
                    <Route exact path="/">
                        <Home />
                    </Route>
                    <Route path="/about">
                        <About />
                    </Route>
                    <Route path="/topics">
                        <Users />
                    </Route>
                </Switch>
            </div>
		</Router>
	);
}

export default App;
