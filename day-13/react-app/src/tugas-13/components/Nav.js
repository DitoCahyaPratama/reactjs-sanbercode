import React from 'react';
import { Link } from 'react-router-dom'

export default function Nav() {
	return (
		<header className="App-header">
			<ul>
				<li>
					<Link to="/">Home</Link>
				</li>
				<li>
					<Link to="/about">About</Link>
				</li>
				<li>
					<Link to="/topics">Users</Link>
				</li>
			</ul>
		</header>
	);
}
