import React from 'react';
import '../App.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Home, About, Users } from './views'

function App() {
	return (
		<Router>
			<div className="App">
				<header className="App-header">
					<ul>
						<li><Link to="/">Home</Link></li>
						<li><Link to="/about">About</Link></li>
						<li><Link to="/topics">Users</Link></li>
					</ul>
				</header>
                <Switch>
                    <Route exact path="/">
                        <Home />
                    </Route>
                    <Route path="/about">
                        <About />
                    </Route>
                    <Route path="/topics">
                        <Users />
                    </Route>
                </Switch>
            </div>
		</Router>
	);
}

export default App;
