import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'

export default function About() {
    let { topicId } = useParams()

    useEffect(() => {
        window.scrollTo(0,window.innerHeight,'smooth')
    }, [])

    return (
        <div className="App">
            <header className="App-header">
                Topik yang anda pilih adalah {topicId}
            </header>
        </div>
    )
}
