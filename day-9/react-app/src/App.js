import React from "react";
import "./App.css";

function App() {
  const numbers = [1, 3, 5, 7, 9, 11];
  const array = [
    {
      nama: "Bejo",
    },
    {
      name: "Jhonson",
    },
    {
      jenengan: "Budi",
    },
    {
      nami: "Aminah",
    },
  ];

  return (
    <div className="App">
      <header className="App-header">
        {/* {
            numbers.map((element, index) => {
              return(
                <div key={index}>
                  <h1>Elemen ke - {index}</h1>
                  <p>{element}</p>
                </div>
              )
            })
          } */}

        {array.map(({nama, name, nami, jenengan}, index) => {
          return (
            <p key={index}>
              {nama}
              {nami}
              {name}
              {jenengan}
            </p>
          );
        })}
      </header>
    </div>
  );
}

export default App;
