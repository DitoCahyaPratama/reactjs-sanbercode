import React, { Component } from "react";

class soal4 extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this)
    this.inputNama = React.createRef()
    this.inputGaji = React.createRef()
    this.inputAlamat = React.createRef()
    this.inputGender = React.createRef()
    this.inputSkills = React.createRef()
    this.inputSlip = React.createRef()
  }
  onSubmit(event) {
    event.preventDefault();
    event.stopPropagation();
    console.log(
      `Namaku adalah ${this.inputNama.current.value}, 
      aku memiliki gaji bulanan sebesar ${this.inputGaji.current.value}. 
      Dan rumahku di ${this.inputAlamat.current.value}
      ${this.inputGender.current.checked}
      ${this.inputSkills.current.value}
      ${this.inputSlip.current.files[0].name}`
    );
  }
  
  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <div>
          <label>Nama</label>
          <div>
            <input type="text" name="nama" ref={this.inputNama} />
          </div>
        </div>
        <div>
          <label>Gaji</label>
          <div>
            <input type="number" name="gaji" ref={this.inputGaji} />
          </div>
        </div>
        <div>
          <label>Gender</label>
          <div>
            <input type="radio" name="gender" value="Laki-laki" ref={this.inputGender}/>
            Laki - laki
            <input type="radio" name="gender" value="Perempuan" ref={this.inputGender}/>
            Perempuan
          </div>
        </div>
        <div>
          <label>Skills</label>
          <div>
            <select ref={this.inputSkills} multiple>
              <option value="">Pilih Skill Anda</option>
              <option value="frontend">Front End Developer</option>
              <option value="backend">Back End Developer</option>
            </select>
          </div>
        </div>
        <div>
          <label>Alamat</label>
          <div>
            <textarea name="alamat" ref={this.inputAlamat} />
          </div>
        </div>
        <div>
            <label>Slip Gaji</label>
            <div>
                <input type="file" name="slipGaji" ref={this.inputSlip}/>
            </div>
        </div>
        <button type="submit">Submit</button>
      </form>
    );
  }
}

export default soal4;
