import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
// import Soal3 from './Soal3';
import Soal4 from './Soal4';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <Soal4 />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
