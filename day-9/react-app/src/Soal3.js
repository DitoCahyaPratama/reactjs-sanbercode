import React, { Component } from "react";

class soal3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nama: "",
      gaji: "",
      skills: [],
      gender: "",
      alamat: "",
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onPick = this.onPick.bind(this);
  }
  onSubmit(event) {
    event.preventDefault()
    event.stopPropagation()
    const {nama, gaji, alamat} = this.state
    alert(`Namaku adalah ${nama}, aku memiliki gaji bulanan sebesar ${gaji}. Dan rumahku di ${alamat}`)
  }
  onChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }
  onPick(event){
    let skills = [...this.state.skills]
    let index = skills.findIndex(elemen => event.target.value === elemen)
    if(index > 0){
        skills = [...skills.slice(0, index), ...skills.slice(index + 1, skills.length)]
    }else if(index === 0){
        skills = [...skills.slice(index + 1, skills.length)]
    }else{
        skills.push(event.target.value)
    }
    this.setState({ skills })
  }
  render() {
    const { nama, gaji, gender, skills, alamat } = this.state;
    return (
      <form onSubmit={this.onSubmit}>
        <div>
          <label>Nama</label>
          <div>
            <input
              type="text"
              name="nama"
              value={nama}
              onChange={this.onChange}
            />
          </div>
        </div>
        <div>
          <label>Gaji</label>
          <div>
            <input
              type="number"
              name="gaji"
              value={gaji}
              onChange={this.onChange}
            />
          </div>
        </div>
        <div>
          <label>Gender</label>
          <div>
            <input
              type="radio"
              name="gender"
              value="Laki-laki"
              checked={gender === "Laki-laki"}
              onChange={this.onChange}
            />{" "}
            Laki - laki
            <input
              type="radio"
              name="gender"
              value="Perempuan"
              checked={gender === "Perempuan"}
              onChange={this.onChange}
            />{" "}
            Perempuan
          </div>
        </div>
        <div>
          <label>Skills</label>
          <div>
            <select onChange={this.onPick} value={skills} multiple>
                <option value="">Pilih Skill Anda</option>
                <option value="frontend">Front End Developer</option>
                <option value="backend">Back End Developer</option>
            </select>
          </div>
        </div>
        <div>
          <label>Alamat</label>
            <div>
                <textarea name="alamat" onChange={this.onChange}>{alamat}</textarea>
            </div>
        </div>
        <button type="submit">Submit</button>
      </form>
    );
  }
}

export default soal3;
