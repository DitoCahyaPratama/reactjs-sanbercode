// /**
//  *
//  * Array methods
//  *
//  */


// const array = [1, 2, 3, 4];

// // push
// array.push(5);

// console.log(array);
// console.log('length', array.length);

// // forEach
// array.forEach((elemen, index) => console.log(`elemen ke-${index}`, elemen));

// // map
// const newArray = array.map((elemen, index) => `elemen ke-${index} adalah ${elemen}`);

// console.log(newArray);

// // pop
// let removedElemen = array.pop();
// console.log(array);
// console.log('removed', removedElemen);

// // shift
// let removedFirstElemen = array.shift();
// console.log(array);
// console.log('removed first', removedFirstElemen);

// // concat
// const mergeArray = newArray.concat(array.map((elemen, index) => `elemen ke-${newArray.length + index} adalah ${elemen}`));
// console.log(mergeArray);

// // filter
// const filteredArray = mergeArray.filter((elemen, index) => index > 2);
// console.log('filtered', filteredArray);

// // reverse
// const reversedArray = array.reverse();
// console.log(reversedArray);

// // sort
// const sortedArray = array.sort((a, b) => b - a);
// console.log(sortedArray);





























// /**
// *
// * Object Destructuring
// *
// */

// const address = {
//     jalan: 'Jl. Tebet Timur',
//     no: '12',
//     kelurahan: 'Tebet',
//     kecamatan: 'Tebet',
//     kota: 'Jakarta Selatan',
//     provinsi: 'Jakarta',
//     negara: 'Indonesia'
// };

// // const jl = address.jalan;
// // const no = address.no;
// // const kel = address.kelurahan;
// // const kec = address.kecamatan;

// const { jalan: jl, no, kelurahan: kel, kecamatan: kec, kota: kt, provinsi: prov, negara: neg } = address;
// const { jalan: jalaannn } = address;

// console.log(jalaannn)
// console.log('address', `${jl} no. ${no}, kel. ${kel}, kec. ${kec}, kota ${kt}, ${prov}, ${neg}`);
































// /**
// *
// * Spread Operator
// *
// */

// const angka = [1, 2, 3, 4, 5];

// const angkaBaru = [-3, -2, -1, 0];

// const angkaLain = [6, 7, 8, 9];

// const mergeAngka = [...angkaBaru, ...angka, ...angkaLain];

// console.log(mergeAngka)

// const object = {
//     jalan: 'Jl. Tebet Timur',
//     no: '12'
// };

// const objectLain = {
//     kelurahan: 'Tebet',
//     kecamatan: 'Tebet',
//     kota: 'Jakarta Selatan'
// };

// const mergeObject = { ...objectLain, ...object, ...mergeAngka };

// mergeAngka[0];
// object['jalan'];

// console.log(mergeObject)



























// Soal 1 

// const arraySatu = [1, 2, 3, 4];
// const arrayDua = [5, 6, 7, 8];
// const arrayTiga = [9, 10, 11, 12];
// const arrayEmpat = [13, 14, 15, 16];

// const mergeAngka = arraySatu.concat(arrayDua, arrayTiga, arrayEmpat);
// const mergeAngka2 = arraySatu.concat(arrayDua).concat(arrayTiga).concat(arrayEmpat);
// const mergeAngka3 = [...arraySatu, ...arrayDua, ...arrayTiga, ...arrayEmpat];

// Array.prototype.unshift.apply(arrayDua, arraySatu)
// Array.prototype.unshift.apply(arrayEmpat, arrayTiga);
// Array.prototype.unshift.apply(arrayEmpat, arrayDua);

// console.log(mergeAngka);
// console.log(mergeAngka2);
// console.log(mergeAngka3); 
// console.log(arrayEmpat);




// Soal 2

// const objectBebas = {
//     aspal: 'hitam',
//     roket: 'meluncur',
//     joker: 'villain',
//     unta: 'arab',
//     ninja: 'hatori'
// }

// const { aspal:a, roket:r, joker:j, unta:u, ninja:n } = objectBebas

// console.log(`aspal: ${n}, roket: ${r}, joker: ${j}, unta: ${u}, ninja: ${n}`);




// Soal 3

const unfilteredArray = [{
        name: 'Bejo',
        isAllowed: true
    },
    {
        name: 'Jhonson',
        isAllowed: false
    },
    {
        name: 'Juara',
        isAllowed: true
    },
    {
        name: 'Panco',
        isAllowed: false
    },
    {
        name: 'Nasional',
        isAllowed: true
    }
]


const bejoJuaraNasional = unfilteredArray.filter((element, index) => element.isAllowed == true );

console.log(bejoJuaraNasional);