export const fungsi = () => { };

export default class Benda {
    constructor(nama, fungsiNama) {
        this.nama = nama;
        this.fungsiNama = fungsiNama;
    }

    fungsi() {
        console.log(`${this.nama} berfungsi untuk ${this.fungsiNama}`)
    }
}