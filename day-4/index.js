/**
 *
 * Classes
 *
 */

// class Benda {
//     constructor(nama, fungsiNama) {
//         this.nama = nama;
//         this.fungsiNama = fungsiNama;
//     }

//     fungsi() {
//         console.log(`${this.nama} berfungsi untuk ${this.fungsiNama}`)
//     }
// }

// const oven = new Benda('Oven', 'memanaskan makanan');
// const payung = new Benda('Payung', 'melindungi dari panas');


























/**
 *
 * Inheritance
 *
 */

// class Benda {
//     constructor(nama, fungsiNama) {
//         this.nama = nama;
//         this.fungsiNama = fungsiNama;
//     }

//     fungsi() {
//         console.log(`${this.nama} berfungsi untuk ${this.fungsiNama}`)
//     }
// }

// class Buku extends Benda {
//     constructor(nama, fungsiNama, halaman, warnaCover) {
//         super(nama, fungsiNama)
//         this.halaman = halaman;
//         this.warnaCover = warnaCover;
//     }

//     tampilkanWarnaCover() {
//         console.log(`Warna dari ${this.nama} adalah ${this.warnaCover}`);
//     }
// }

// const buku = new Buku('Buku Gambar', 'menggambar', '15', 'biru');
// buku.tampilkanWarnaCover();
































/**
 *
 * Modules
 *
 */

// import { Buku } from './buku.js';

// const buku = new Buku('Buku Gambar', 'menggambar', '15', 'biru');
// buku.tampilkanWarnaCover();




























/**
 *
 * Named and Default Exports
 *
 */











// class MakhlukGhaib {
//     constructor() {
//         this.nama = 'Pocong';
//         this.kekuatan = 'Terbang';
//     }
//     attack() {
//         console.log('Senyum');
//     }
// };
// class Benda {
//     constructor(nama = 'kompor', bahan = 'besi', actionName = 'memanaskan') {
//         this.nama = nama;
//         this.bahan = bahan;
//         this.actionName = actionName;
//     }
//     action() {
//         console.log(this.actionName);
//     }
// };

// class HalGhaibLain {
//     constructor(nama = 'cinta', bahan = 'perjuangan bersama', actionName = 'Membara') {
//         this.nama = nama;
//         this.bahan = bahan;
//         this.actionName = actionName;
//     }
//     action() {
//         console.log(this.actionName);
//     }
// };

// class BendaLain extends Benda {
//     constructor(dasar, nama, bahan, actionName) {
//         super(nama, bahan, actionName);
//         this.dasar = dasar;
//     }
//     actionLain() {
//         console.log("ini action lain");
//     }
// }

// const barang = new Benda('cinta', 'perjuangan bersama', 'Membara');
// console.log(barang);

// const bendaLainnya = new BendaLain('ritual', 'Jailangkung', 'kayu', 'nyanyi');
// console.log(bendaLainnya);

// const bendaLainnya1 = new BendaLain('ritual');
// console.log(bendaLainnya1);


// Soal no 2


// import { NamedExport as AliasNamed } from './contoh.js';

// const alias = new AliasNamed('ini nama alias');
// console.log(alias);


// Soal no 3

// import { Tes } from '../day-3/tes.js';

// const test = new Tes('testing');
// console.log(test)


// Soal no 4

// import HelloCat from './HelloCat.js';

// const test = new HelloCat();
// console.log(test)


// Soal no 5

import { default as us } from './user.js';

const test = new us('testing');
console.log(test)
