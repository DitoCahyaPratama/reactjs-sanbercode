import Benda, { fungsi } from './benda.js'

export class Buku extends Benda {
    constructor(nama, fungsiNama, halaman, warnaCover) {
        super(nama, fungsiNama)
        this.halaman = halaman;
        this.warnaCover = warnaCover;
    }

    tampilkanWarnaCover() {
        console.log(`Warna dari ${this.nama} adalah ${this.warnaCover}`);
    }
}