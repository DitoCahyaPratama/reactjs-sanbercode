import React from "react";
import ReactDOM from "react-dom";
// import './index.css';
// import App from './App';
import * as serviceWorker from "./serviceWorker";

// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();

// Soal3

// function Welcome(props) {
//   return <h1>Hello, {props.name}</h1>;
//  }

//  function App() {
//   return (
//     <div>
//       <Welcome name="Dito" />
//     </div>
//   );
//  }

//  ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );
// serviceWorker.unregister();

// Soal 4


// function UserInfo(props) {
//   return (
//     <div className="UserInfo">
//       <img
//         className="Avatar"
//         src={props.author.avatarUrl}
//         alt={props.author.name}
//       />
//       <div className="UserInfo-name">{props.author.name}</div>
//     </div>
//   );
// }

// function Comment(props) {
//   return (
//     <div className="Comment">
//       <UserInfo author={props} />
//       <div className="Comment-text">{props.text}</div>
//       {/* <div className="Comment-date">{formatDate(props.date)}</div> */}
//     </div>
//   );
// }

//  ReactDOM.render(
//   <React.StrictMode>
//     <Comment name="dito" text="Baik"/>
//   </React.StrictMode>,
//   document.getElementById('root')
// );
// serviceWorker.unregister();


function Comment(props) {
  return (
    <React.Fragment>
      <div className="UserInfo">
        <img className="Avatar"
          src={props.author.avatarUrl}
          alt={props.author.name}
        />
        <div className="UserInfo-name">
          {props.author.name}
        </div>
      </div>
      <div className="Comment-text">
        {props.text}
      </div>
      <div className="Comment-date">
        {/* {formatDate(props.date)} */}
      </div>
    </React.Fragment>
  );
 }
 
 