const initialState = {
    counter: 0,
}

const counters = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD COUNTER':
            return {
                counter: state.counter+1
            }
        case 'SUBSTRACT COUNTER':
            return {
                counter: state.counter-1
            }
        default:
            return state;
    }
}

export default counters