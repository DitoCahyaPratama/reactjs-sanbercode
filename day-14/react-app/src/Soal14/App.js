import React from 'react'
import { useDispatch, useSelector } from 'react-redux'

function App() {
    const dispatch = useDispatch()
    const counters = useSelector(state => state.counters.counter)
    
    const _handleSubstract = () => {
        dispatch({
            type: 'SUBSTRACT COUNTER',          
        })
    }

    const _handleAdd = () => {
        dispatch({
            type: 'ADD COUNTER',          
        })
    }

    return (
        <div className="App App-header">
            <div>Counter : {counters}</div>
            <div>
                <button onClick={_handleSubstract}>Substract</button>
                <button onClick={_handleAdd}>Add</button>
            </div>
        </div>
    )
}

export default App