import React, { Component } from 'react'


class TimeInput extends Component {
    _handleChange = (e) => {
        this.props.onChangeTime(e.target.value)
    }
    render() {
        const{ timeLabel, time } = this.props
        return (
            <>
                <label>{timeLabel === "m" ? "Minutes" : "Seconds"}</label>
                <div>
                    <input type="number" value={time} onChange={this._handleChange}/>
                </div>
            </>
        )
    }
}


class Soal1 extends Component {
    constructor(props){
        super(props)
        this.state = {
            menit: 0,
            detik: 0
        }
    }
    onSubmit = (e) => {
        e.preventDefault();
        alert(`detik: ${this.state.detik} sama dengan menit: ${this.state.menit}`);
      };
    onChangeMinutes = (menit) => {
        // let detik = (parseFloat(menit) * 60).toString()
        let detik = (parseInt(menit) * 60).toString();
        this.setState({ menit, detik })
    }
    onChangeSeconds = (detik) => {
        let menit = (parseInt(detik) / 60).toString()
        this.setState({ menit, detik })
    }
    render() {
        const {menit, detik} = this.state
        return (
            <form onSubmit={this.onSubmit}>
                <TimeInput timeLabel="s" time={detik} onChangeTime={this.onChangeSeconds} />
                <TimeInput timeLabel="m" time={menit} onChangeTime={this.onChangeMinutes} />
                <br />
                <button type="submit">Convert!</button>
            </form>
        )
    }
}

export default Soal1