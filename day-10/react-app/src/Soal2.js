import React, { Component } from "react";

class WeightInput extends Component {
  _handleChange = (e) => {
    this.props.onChangeWeight(e.target.value);
  };
  render() {
    const { weightLabel, weight } = this.props;
    return (
      <>
        <label>{weightLabel === "gr" ? "Gram" : "KiloGram"}</label>
        <div>
          <input type="number" value={weight} onChange={this._handleChange} />
        </div>
      </>
    );
  }
}

class Soal2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gram: 0,
      kilogram: 0,
    };
  }
  onSubmit = (e) => {
    e.preventDefault();
    alert(`gram: ${this.state.gram} sama dengan kilogram: ${this.state.kilogram}`);
  };
  onChangeGram = (gram) => {
    let kilogram = (parseFloat(gram) / 1000).toString();
    this.setState({ gram, kilogram });
  };
  onChangeKiloGram = (kilogram) => {
    let gram = (parseFloat(kilogram) * 1000).toString();
    this.setState({ gram, kilogram });
  };
  render() {
    const { gram, kilogram } = this.state;
    return (
      <form onSubmit={this.onSubmit}>
        <WeightInput
          weightLabel="gr"
          weight={gram}
          onChangeWeight={this.onChangeGram}
        />
        <WeightInput
          weightLabel="kg"
          weight={kilogram}
          onChangeWeight={this.onChangeKiloGram}
        />
        <br />
        <button type="submit">Convert!</button>
      </form>
    );
  }
}

export default Soal2;
