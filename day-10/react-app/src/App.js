import React, { Component } from "react";

class CurrencyInput extends Component {
  _handleChange = (e) => {
    this.props.onChangeCurrency(e.target.value);
  };

  render() {
    const { curLabel, currency } = this.props;
    return (
      <>
        <label>{curLabel === "Rp" ? "Rupiah:" : "Dollar:"}</label>
        <div>
          <input type="number" value={currency} onChange={this._handleChange} />
        </div>
      </>
    );
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rupiah: 0,
      dollar: 0,
    };
  }
  onSubmit = (e) => {
    e.preventDefault();
    alert(`Rupiah: ${this.state.rupiah} dengan dollar: ${this.state.dollar}`);
  };
  onChangeRupiah = (rupiah) => {
    let dollar = (parseFloat(rupiah) / 14272.05).toString();
    this.setState({ rupiah, dollar });
  };
  onChangeDollar = (dollar) => {
    let rupiah = (parseFloat(dollar) * 14272.05).toString();
    this.setState({ rupiah, dollar });
  };
  render() {
    const { rupiah, dollar } = this.state;
    return (
      <form onSubmit={this.onSubmit}>
        <CurrencyInput
          curLabel="Rp"
          currency={rupiah}
          onChangeCurrency={this.onChangeRupiah}
        />
        <CurrencyInput
          curLabel="$"
          currency={dollar}
          onChangeCurrency={this.onChangeDollar}
        />
        <br />
        <button type="submit">Convert!</button>
      </form>
    );
  }
}

export default App;
