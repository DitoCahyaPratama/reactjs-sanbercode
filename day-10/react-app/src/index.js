import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
// import Soal1 from './Soal1'
import Soal2 from './Soal2'
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <Soal2 />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
