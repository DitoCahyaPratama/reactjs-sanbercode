import React, { useState, useEffect } from "react";
import "./App.css";

function App(props) {

  const [myState, setMyState] = useState("Hello!")

  //componentDidMount
  useEffect(() => {
    setMyState("Hello World!");
  }, []);

  //componentDidUpdate
  useEffect(() => {
    console.log(`my updated state is ${myState}`);
  });

  //ComponentWillUnmount
  useEffect(() => {
    return () => {
      console.log("Good Bye!");
    };
  }, []);

  return <div className="hello">Halo Dunia</div>;
}

export default App;