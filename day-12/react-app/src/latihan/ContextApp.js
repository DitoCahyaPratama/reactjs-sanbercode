import React, { Component } from "react";
import { ThemeContext } from "./themeContext";
import MyComponent from "./components/MyComponents";

class ContextApp extends Component {
  constructor() {
    super();
    this.onChangeTheme = () => {
      this.setState((prevState) => ({
        colorTheme: prevState.colorTheme === "dark" ? "white" : "dark",
      }));
    };
    this.state = {
      colorTheme: "dark",
      onChangeTheme: this.onChangeTheme
    };
  }

  render() {
    return (
      <ThemeContext.Provider value={this.state}>
        <MyComponent />
      </ThemeContext.Provider>
    );
  }
}

export default ContextApp;
