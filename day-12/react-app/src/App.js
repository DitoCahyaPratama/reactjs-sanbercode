import React from 'react';
import './App.css';
// import Soal1 from './Soal1'
// import ContextApp from './latihan/ContextApp'
import Soal2 from './soal2/ContextApp'

function App() {
  return (
    // <Soal1 />
    // <ContextApp />
    <Soal2 />
  );
}

export default App;
