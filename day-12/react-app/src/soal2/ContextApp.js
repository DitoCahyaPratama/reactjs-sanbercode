import React, { useState } from "react";
import { ThemeContext } from "./themeContext";
import MyComponent from "./components/MyComponents";

export default function ContextApp() {
  const onChangeTheme = () => {
    setState((prevState) => ({
      colorTheme: prevState.colorTheme === "dark" ? "white" : "dark",
      language: prevState.language === "en" ? "id" : "en",
      onChangeTheme: onChangeTheme,
    }));
  };

  const [state, setState] = useState({
    colorTheme: "dark",
    language: "en",
    onChangeTheme: onChangeTheme,
  });

  return (
    <ThemeContext.Provider value={state}>
      <MyComponent />
    </ThemeContext.Provider>
  );
}
