import {createContext} from 'react'

export const ThemeContext = createContext({
    colorTheme: 'dark',
    language: 'en',
    onChangeTheme: () => {}
})