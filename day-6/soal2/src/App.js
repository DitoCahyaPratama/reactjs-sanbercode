import React from 'react';

function App() {
  return (
    <div>
      <h1>Hello Bejo!</h1>
      <h3>Welcome to Cat World!</h3>
      <p>To order my food, please fill the form below.</p>
      <form>
        <p>Name</p>
        <input type="text" name="name" placeholder="name..." required />
        <p>Delivery Date</p>
        <input type="date" name="date" placeholder="name..." required />
        <p>Address</p>
        <textarea name="address" id="" cols="30" rows="5"></textarea>
        <p>Quantity</p>
        <input type="number" name="quantity" placeholder="quantity..." required />  
      </form>
    </div>
  );
}

export default App;
