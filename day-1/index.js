let myLetVariable = 'hai cat!!';

var myVarTrueVariable = true;

var myVarFalseVariable = false;

var myVarNullVariable = null;

const myConstVariable = -1.2;



var myVarVariable; // ini valuenya adalah undefined

// object
var myObjectVariable = {
    'key': 'value',
    key2: true,
    key3: null,
    key4: 5,
    key6: () => { },
    key5: {
        key: 'value lain'
    }
};

// array
var myAnotherObjectVariable = [1, 'hai cat!', true, { name: 'Arjun Sumarlan' }];


// f(x, y) = x^2 + y

function suatuFungsi(x, y) {
    let ayok = 'ayok!';
    console.log(ayok);
    console.log(x ^ 2 + y);
}

suatuFungsi(2, 3);

{
    var x = 5;
}

console.log(x);

const konstanta = {
    name: 'Arjun Sumarlan'
};

console.log(konstanta) // sebelum

konstanta.name = 'Cintya';

console.log(konstanta) // setelah

let bukanKonstanta = 'hai';

bukanKonstanta = 5;

console.log(bukanKonstanta);


// let itu mendeklarasikan variable hanya di scopenya.

// var itu mendeklarasikan variable di scope terdekat.




const sebuahObject = {
    fungsi: () => {
        console.log('Hai Dunia Kucing!!')
    }
}

sebuahObject.fungsi();


// var sebuahVariabel = 'apple'
// var sebuahVariabel = 'orange' // SyntaxError: Identifier 'sebuahVariabel ' has already been declared
// console.log(sebuahVariabel)


const sebuahVariabel  = {}
sebuahVariabel.color = 'red'

console.log(sebuahVariabel);


var haloHalo = 1
var haloHaloBandung = function() {
  console.log(haloHalo)
//   var haloHalo = 2
}

haloHaloBandung()


function hoist() {
    console.log(message);
    var message='Hoisting is all the rage!'
  }
  
  hoist();
